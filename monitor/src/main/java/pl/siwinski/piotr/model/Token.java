package pl.siwinski.piotr.model;

import pl.siwinski.piotr.config.Config;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.PriorityQueue;
import java.util.Queue;

public class Token implements Serializable {
  private Map<String, Integer> lastRequestMap = new HashMap<>();
  private Queue<String> waitingQueue = new PriorityQueue<>();
  private Buffer buffer;

  Token(Config config) {
    this.buffer = new Buffer(config.getBufferSize());
    this.lastRequestMap.put(getIdFromAddress(config.getHostAddress()), 0);
    for (String s : config.getNodeAddresses()) {
      this.lastRequestMap.put(getIdFromAddress(s), 0);
    }
  }

  public Map<String, Integer> getLastRequestMap() {
    return lastRequestMap;
  }

  private String getIdFromAddress(String address) {
    return address.split(":")[2];
  }

  public Queue<String> getWaitingQueue() {
    return waitingQueue;
  }

  public Buffer getBuffer() {
    return buffer;
  }

  @Override
  public String toString() {
    return String.format("Token{lastRequestMap=%s, waitingQueue=%s}", lastRequestMap, waitingQueue);
  }
}
