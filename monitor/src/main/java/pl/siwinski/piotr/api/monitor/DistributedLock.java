package pl.siwinski.piotr.api.monitor;

import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;
import pl.siwinski.piotr.api.command.CommandExecutor;

public class DistributedLock implements Lock {

  private CommandExecutor commandExecutor;

  DistributedLock(CommandExecutor commandExecutor) {
    this.commandExecutor = commandExecutor;
  }

  @Override
  public void lock() {
    commandExecutor.acquireLock();
  }

  @Override
  public void lockInterruptibly() {
    throw new UnsupportedOperationException();
  }

  @Override
  public boolean tryLock() {
    throw new UnsupportedOperationException();
  }

  @Override
  public boolean tryLock(long time, TimeUnit unit) {
    throw new UnsupportedOperationException();
  }

  @Override
  public void unlock() {
    commandExecutor.releaseLock();
  }

  @Override
  public Condition newCondition() {
    return commandExecutor.newCondition();
  }
}
