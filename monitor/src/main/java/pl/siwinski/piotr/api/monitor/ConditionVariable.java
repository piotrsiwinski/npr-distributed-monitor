package pl.siwinski.piotr.api.monitor;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import pl.siwinski.piotr.model.Message;
import pl.siwinski.piotr.model.MessageType;
import pl.siwinski.piotr.model.Node;

import java.util.Date;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;

public class ConditionVariable implements Condition {
  private Logger logger = LoggerFactory.getLogger(ConditionVariable.class);
  private final Lock localLock;
  private final Condition localCondition;
  private boolean waiting = false;

  public ConditionVariable(Lock localLock, Condition localCondition, Node node) {
    this.localLock = localLock;
    this.localCondition = localCondition;
    this.node = node;
  }

  private final Node node;

  @Override
  public void await() throws InterruptedException {
    logger.info("NodeId: {} - await on condition", node.getId());
    localLock.lock();
    this.waiting = true;
    while (waiting) {
      localCondition.await();
    }
    localLock.unlock();
  }

  @Override
  public void awaitUninterruptibly() {
    throw new UnsupportedOperationException();
  }

  @Override
  public long awaitNanos(long nanosTimeout) throws InterruptedException {
    throw new UnsupportedOperationException();
  }

  @Override
  public boolean await(long time, TimeUnit unit) throws InterruptedException {
    throw new UnsupportedOperationException();
  }

  @Override
  public boolean awaitUntil(Date deadline) throws InterruptedException {
    throw new UnsupportedOperationException();
  }

  @Override
  public void signal() {
    throw new UnsupportedOperationException();
  }

  @Override
  public void signalAll() {
    logger.info("NodeId: {} - Sending signalAll", node.getId());
    node.sendToAll(Message.builder().id(node.getId()).messageType(MessageType.SIGNAL).build());
  }

  public void wakeUp() {
    this.waiting = false;
    this.localCondition.signalAll();
  }

  public boolean isWaiting() {
    return waiting;
  }
}
