package pl.siwinski.piotr.model;

public enum MessageType {
  OK,
  ACQUIRE_LOCK,
  RELEASE_LOCK,
  SIGNAL,
  AWAIT
}
