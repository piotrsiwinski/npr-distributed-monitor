package condition.prod;

import pl.siwinski.piotr.api.monitor.Monitor;
import pl.siwinski.piotr.api.monitor.MonitorService;
import pl.siwinski.piotr.config.Config;

public class ProdProgram {
  public static void main(String[] args) {
    Monitor monitor =
        MonitorService.create(
            Config.builder()
                .hostAddress("tcp://*:5001")
                .nodeAddress("tcp://*:5000")
                .ioThreads(10)
                .bufferSize(10)
                .build());

    System.out.println("Starting producer process");

    Producer producer = new Producer(monitor);
    int number = 0;
    while (true) {
      producer.produce(number);
      number = (number + 1) % 20;
      try {
        Thread.sleep(1000);
      } catch (InterruptedException e) {
        e.printStackTrace();
      }
    }
  }
}
