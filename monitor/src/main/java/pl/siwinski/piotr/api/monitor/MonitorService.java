package pl.siwinski.piotr.api.monitor;

import pl.siwinski.piotr.api.command.CommandExecutor;
import pl.siwinski.piotr.config.Config;
import pl.siwinski.piotr.model.Buffer;
import pl.siwinski.piotr.model.Node;

import javax.naming.OperationNotSupportedException;
import java.util.Collection;
import java.util.List;
import java.util.Objects;
import java.util.concurrent.locks.Lock;

public class MonitorService implements Monitor {

  private CommandExecutor commandExecutor;

  protected MonitorService(CommandExecutor commandExecutor) {
    this.commandExecutor = Objects.requireNonNull(commandExecutor);
  }

  public static Monitor create(Config config) {
    return new MonitorService(new CommandExecutor(new Node(config)));
  }

  @Override
  public Lock getLock() {
    return new DistributedLock(commandExecutor);
  }

  @Override
  public Buffer getBuffer() {
    return this.commandExecutor.getBuffer();
  }

  @Override
  public void close() {
    this.commandExecutor.shutdown();
  }
}
