## Distributed monitor
An implementation of a monitor synchronization construct working in the distributed environment. Implemented with ZeroMQ and Java and uses Suzuki-Kasami algorithm

### Build prerequisites
```Java 8 or higher``` 

### How to use
All what you need to do is create Monitor instance and set hostAddress and nodeAddresses
You need to specify your host address and all other host addresses
```
Monitor monitor =
        MonitorService.create(
            Config.builder()
                .hostAddress("tcp://*:5000")
                .nodeAddress("tcp://*:5001")
                .ioThreads(10)
                .bufferSize(10)
                .build());

```