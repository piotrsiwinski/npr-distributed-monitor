package pl.siwinski.piotr.api.command;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import pl.siwinski.piotr.api.monitor.ConditionVariable;
import pl.siwinski.piotr.exception.ReleasingLockWithoutTokenException;
import pl.siwinski.piotr.model.Buffer;
import pl.siwinski.piotr.model.Message;
import pl.siwinski.piotr.model.MessageType;
import pl.siwinski.piotr.model.Node;
import pl.siwinski.piotr.model.Token;

import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.Objects;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

public class CommandExecutor<T> {

  private Logger logger = LoggerFactory.getLogger(CommandExecutor.class);

  private Node node;
  private ExecutorService executorService = Executors.newFixedThreadPool(10);

  private Lock localLock = new ReentrantLock();
  private Condition localCondition = localLock.newCondition();
  private Condition localConditionVariable = localLock.newCondition();
  private boolean isInCriticalSection = false;
  private ConditionVariable distributedCV;

  public CommandExecutor(Node node) {
    this.node = node;
    this.distributedCV = new ConditionVariable(localLock, localConditionVariable, node);
    submitReceiverTask();
  }

  public void acquireLock() {
    localLock.lock();
    logger.info("NodeId: {} - Aquiring lock - trying to get token", node.getId());

    try {
      while (Objects.isNull(node.getToken())) {
        logger.info("NodeId: {} - Waiting for token - lock await", node.getId());
        this.executorService.submit(
            () ->
                this.node.sendToAll(
                    Message.builder()
                        .id(node.getId())
                        .requestNumber(this.node.incrementAndGetHostRequestNumber())
                        .messageType(MessageType.ACQUIRE_LOCK)
                        .build()));
        localCondition.await();
      }
    } catch (InterruptedException e) {
      logger.info("NodeId: {} - Interrupted exception: {}", node.getId(), e.getMessage());
      throw new RuntimeException("Application failure");
    }
    logger.info("NodeId: {} - Acquired distributed lock", node.getId());
    this.isInCriticalSection = true;
    localLock.unlock();
  }

  public void releaseLock() {
    this.localLock.lock();

    logger.info("NodeId: {} - Releasing lock", node.getId());
    if (Objects.isNull(node.getToken())) {
      throw new ReleasingLockWithoutTokenException();
    }
    Token token = node.getToken();
    token.getLastRequestMap().put(node.getId(), node.getRequestsNumberMap().get(node.getId()));
    logger.info("NodeId: {} - Succesfully executed process critical section", node.getId());
    for (String s : node.getRequestsNumberMap().keySet()) {
      if (token.getWaitingQueue().contains(s)) {
        continue;
      }
      if (isOutdatedRequest(s, token)) {
        logger.info(
            "NodeId: {} - Found outdated request - adding process {} to token waiting queue",
            node.getId(),
            s);
        token.getWaitingQueue().add(s);
      }
    }
    if (!token.getWaitingQueue().isEmpty()) {
      String head = token.getWaitingQueue().remove();
      logger.info(
          "NodeId: {} - Waiting queue not empty - sending token to node: {}", node.getId(), head);
      this.sendTokenToNode(head, token);
    } else {
      logger.info(
          "NodeId: {} - Waiting queue empty - process {} keeps token", node.getId(), node.getId());
    }
    this.isInCriticalSection = false;
    this.localLock.unlock();
  }

  public void shutdown() {
    logger.info("NodeId: {} - shutting down node", node.getId());
    this.node.shutdown();
    this.executorService.shutdown();
  }

  public Buffer getBuffer() {
    logger.info("Getting distributed buffer");
    this.localLock.lock();
    Buffer buffer = node.getToken().getBuffer();
    this.localLock.unlock();
    return buffer;
  }

  private void submitReceiverTask() {
    executorService.submit(
        () -> {
          while (true) {
            Message receivedMessage = this.node.receiveMessage();
            logger.info("NodeId: {} - Received message {}", node.getId(), receivedMessage);
            if (receivedMessage.getMessageType() == MessageType.ACQUIRE_LOCK) {
              onAquireLockMessage(receivedMessage);
            } else if (receivedMessage.getMessageType() == MessageType.OK) {
              onMessageOk(receivedMessage);
            } else if (receivedMessage.getMessageType() == MessageType.SIGNAL) {
              logger.info(
                  "NodeId: {} - Received signal from {}", node.getId(), receivedMessage.getId());
              localLock.lock();
              this.distributedCV.wakeUp();
              localLock.unlock();
            }
          }
        });
  }

  private void onAquireLockMessage(Message receivedMessage) {
    localLock.lock();
    Integer localRequestNumber = node.getRequestsNumberMap().get(receivedMessage.getId());
    Integer tokenRequestNumber = receivedMessage.getRequestNumber();
    if (tokenRequestNumber < localRequestNumber) {
      logger.info(
          "NodeId: {} - Noticed outdated request from {}", node.getId(), receivedMessage.getId());
    }

    node.getRequestsNumberMap()
        .put(receivedMessage.getId(), Math.max(localRequestNumber, tokenRequestNumber));

    if (!Objects.nonNull(node.getToken())) {
      localLock.unlock();
      return;
    }
    if (isInCriticalSection && !distributedCV.isWaiting()) {
      logger.info("NodeId: {} - Current in critical section - cannot sent token", node.getId());
      localLock.unlock();
      return;
    }
    logger.info("NodeId: {} - Process is currently not in critical section", node.getId());

    if (isOutdatedRequest(receivedMessage.getId(), node.getToken())) {
      logger.info(
          "NodeId: {} - Indicated outdated request - sending token to process {}",
          node.getId(),
          receivedMessage.getId());
      sendTokenToNode(receivedMessage.getId(), node.getToken());
    }
    logger.info("NodeId{} - Processed request from node {}", node.getId(), receivedMessage.getId());
    localLock.unlock();
  }

  private boolean isOutdatedRequest(String id, Token token) {
    return node.getRequestsNumberMap()
        .get(id)
        .equals((Integer) token.getLastRequestMap().get(id) + 1);
  }

  private void onMessageOk(Message receivedMessage) {
    this.localLock.lock();
    logger.info("NodeId: {} - Received permission to acquire lock", node.getId());
    this.node.setToken(receivedMessage.getToken());
    this.localCondition.signalAll();
    this.localLock.unlock();
  }

  private void sendTokenToNode(String nodeId, Token token) {
    if (Objects.isNull(token)) {
      throw new IllegalArgumentException("Token cannot be null");
    }
    node.sendTo(
        nodeId,
        Message.builder()
            .id(node.getId())
            .messageType(MessageType.OK)
            .token(node.getToken())
            .build());
    node.setToken(null);
  }

  public Condition newCondition() {
    return this.distributedCV;
  }
}
