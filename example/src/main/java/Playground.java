import org.zeromq.ZMQ;

public class Playground {

  public static void main(String[] args) {
    ZMQ.Context context = ZMQ.context(10);
    ZMQ.Socket receiver = context.socket(ZMQ.PULL);
    boolean bindres = receiver.bind("tcp://*:5000");
    System.out.println("BIND RESULT: " + bindres);
    System.out.println("PULL - Listening on port - 5000");
    while (true) {
      String message = new String(receiver.recv());
      System.out.println("Received message: " + message);
    }
  }
}
