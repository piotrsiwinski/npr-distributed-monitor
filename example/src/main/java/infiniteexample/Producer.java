package infiniteexample;

import pl.siwinski.piotr.api.monitor.Monitor;
import pl.siwinski.piotr.api.monitor.MonitorService;
import pl.siwinski.piotr.config.Config;

import java.util.concurrent.locks.Lock;

class Producer {

  public static void main(String[] args) throws InterruptedException {
    Monitor monitor =
        MonitorService.create(
            Config.builder()
                .hostAddress("tcp://*:5000")
                .nodeAddress("tcp://*:5001")
                .ioThreads(10)
                .build());
    System.out.println("Starting producer process");
    Lock lock = monitor.getLock();

    while (true) {
      System.out.println("Trying to get LOCK");
      lock.lock();
      Thread.sleep(1000);
      System.out.println("RELEASING LOCK ");
      lock.unlock();
    }
  }
}
