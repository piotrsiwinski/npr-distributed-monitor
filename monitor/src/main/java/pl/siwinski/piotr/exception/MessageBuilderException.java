package pl.siwinski.piotr.exception;

public class MessageBuilderException extends MonitorException {
  public MessageBuilderException() {}

  public MessageBuilderException(String message) {
    super(message);
  }
}
