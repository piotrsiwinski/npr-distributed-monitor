package condition.prod;

import pl.siwinski.piotr.api.monitor.Monitor;
import pl.siwinski.piotr.model.Buffer;

import java.util.Objects;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;

public class Producer {

  private final Monitor monitor;
  private int producerIndex = 0;
  private Lock lock;
  private Condition condition;

  public Producer(Monitor monitor) {
    this.monitor = Objects.requireNonNull(monitor);
    this.lock = monitor.getLock();
    this.condition = lock.newCondition();
  }

  public void produce(Integer value) {
    Lock lock = monitor.getLock();
    lock.lock();
    Buffer buffer = monitor.getBuffer();

    while (buffer.isFull()) {
      lock.lock();
      System.out.println("Buffer full going to sleep");
      try {
        condition.await();
        lock.lock();
      } catch (InterruptedException e) {
        e.printStackTrace();
      }
      buffer = monitor.getBuffer();
      lock.unlock();
    }
    lock.lock();

    System.out.println("Produced " + value);
    buffer.add(value, producerIndex);
    producerIndex = (producerIndex + 1) % buffer.getBufferSize();
    condition.signalAll();
    System.out.println("Releasing lock");
    lock.unlock();
  }
}
