package pl.siwinski.piotr.exception;

public class MessageMissingNodeIdException extends MessageBuilderException {

  public MessageMissingNodeIdException() {

  }

  public MessageMissingNodeIdException(String message) {
    super(message);
  }
}
