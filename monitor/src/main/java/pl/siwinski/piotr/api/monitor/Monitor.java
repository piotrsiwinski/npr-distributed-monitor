package pl.siwinski.piotr.api.monitor;

import pl.siwinski.piotr.model.Buffer;

import java.util.concurrent.locks.Lock;

public interface Monitor {
  Lock getLock();

  Buffer getBuffer();

  void close();
}
