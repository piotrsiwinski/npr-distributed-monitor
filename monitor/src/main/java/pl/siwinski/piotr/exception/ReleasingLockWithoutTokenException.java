package pl.siwinski.piotr.exception;

public class ReleasingLockWithoutTokenException extends IllegalMonitorStateException {
  public ReleasingLockWithoutTokenException() {
  }

  public ReleasingLockWithoutTokenException(String message) {
    super(message);
  }
}
