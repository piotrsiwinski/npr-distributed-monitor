package condition.cons;

import pl.siwinski.piotr.api.monitor.Monitor;
import pl.siwinski.piotr.model.Buffer;

import java.util.Objects;
import java.util.Optional;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;

public class Consumer {
  private final Monitor monitor;
  private int consumerIndex = 0;
  private Lock lock;
  private Condition condition;

  public Consumer(Monitor monitor) {
    this.monitor = Objects.requireNonNull(monitor);
    this.lock = monitor.getLock();
    this.condition = lock.newCondition();
  }

  public Optional<Integer> consume() {
    lock.lock();

    Buffer buffer = monitor.getBuffer();
    while (buffer.isEmpty()) {
      lock.lock();
      System.out.println("Nothing to consume - await");
      try {
        condition.await();
        lock.lock();
      } catch (InterruptedException e) {
        e.printStackTrace();
      }
      buffer = monitor.getBuffer();
      lock.unlock();
    }
    lock.lock();
    System.out.println("WAKED UP - SIGNAL RECEIVED");
    Integer consumedValue = buffer.remove(consumerIndex);
    System.out.println("Consumed" + consumedValue);
    consumerIndex = (consumerIndex + 1) % buffer.getBufferSize();
    System.out.println("CONSUMED SENDING SIGNAL TO PRODUCER");
    condition.signalAll();
    lock.unlock();
    System.out.println("Releasing lock");
    return Optional.of(consumedValue);
  }
}
