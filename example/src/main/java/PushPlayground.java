import org.zeromq.ZMQ;

public class PushPlayground {

  public static void main(String[] args) throws InterruptedException {
    ZMQ.Context context = ZMQ.context(10);
    ZMQ.Socket sender = context.socket(ZMQ.PUSH);
    boolean connect = sender.connect("tcp://*:5000");
    System.out.println("RESULT OF CONNECT: " + connect);

    int number = 0;
    while (true) {
      System.out.println("Sending message no: " + (++number));
      Thread.sleep(1000);
      sender.send("MESSAGE - NODE 1 " + number);
    }


  }
}
