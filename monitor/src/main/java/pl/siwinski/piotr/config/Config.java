package pl.siwinski.piotr.config;

import java.util.ArrayList;
import java.util.List;

public class Config {
  private final String hostAddress;
  private final List<String> nodeAddresses;
  private final int ioThreads;
  private final int bufferSize;

  protected Config(String hostAddress, List<String> nodeAddresses, int ioThreads, int bufferSize) {
    this.hostAddress = hostAddress;
    this.nodeAddresses = nodeAddresses;
    this.ioThreads = ioThreads;
    this.bufferSize = bufferSize;
  }

  public String getHostAddress() {
    return hostAddress;
  }

  public List<String> getNodeAddresses() {
    return nodeAddresses;
  }

  public int getIoThreads() {
    return ioThreads;
  }

  public int getBufferSize() {
    return bufferSize;
  }

  public static ConfigBuilder builder() {
    return new ConfigBuilder();
  }

  public static class ConfigBuilder {
    private String hostAddress;
    private List<String> nodeAddresses = new ArrayList<>();
    private int ioThreads;
    private int bufferSize;

    public ConfigBuilder hostAddress(String hostAddress) {
      this.hostAddress = hostAddress;
      return this;
    }

    public ConfigBuilder nodeAddress(String address) {
      this.nodeAddresses.add(address);
      return this;
    }

    public ConfigBuilder ioThreads(int ioThreadsNumber) {
      this.ioThreads = ioThreadsNumber;
      return this;
    }

    public ConfigBuilder bufferSize(int bufferSize) {
      this.bufferSize = bufferSize;
      return this;
    }

    public Config build() {
      return new Config(this.hostAddress, this.nodeAddresses, this.ioThreads, this.bufferSize);
    }
  }
}
