package pl.siwinski.piotr.model;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import pl.siwinski.piotr.config.Config;

import static org.assertj.core.api.AssertionsForInterfaceTypes.assertThat;

public class NodeTest {

  Node prod;
  Node cons;

  @Before
  public void setUp() {
    this.prod =
        new Node(
            Config.builder()
                .hostAddress("tcp://*:5000")
                .nodeAddress("tcp://*:5001")
                .ioThreads(10)
                .build());

    this.cons =
        new Node(
            Config.builder()
                .hostAddress("tcp://*:5001")
                .nodeAddress("tcp://*:5000")
                .ioThreads(10)
                .build());
  }

  @After
  public void cleanUp() {
    this.prod.shutdown();
    this.cons.shutdown();
  }

  @Test
  public void should_increment_request_number(){

    prod.incrementAndGetHostRequestNumber();
    assertThat(prod.getRequestsNumberMap()).extracting(prod.getId()).contains(1);
    assertThat(prod.getRequestsNumberMap()).extracting(cons.getId()).contains(0);
  }

  @Test
  public void should_have_id_as_port_number() {
    assertThat(prod.getId()).isEqualTo("5000");
    assertThat(cons.getId()).isEqualTo("5001");
  }

  @Test(timeout = 10000L)
  public void should_find_node_by_id() {

    boolean result =
        prod.sendTo(
            cons.getId(), Message.builder().id(prod.getId()).messageType(MessageType.OK).build());
    Message message = cons.receiveMessage();
    assertThat(result).isTrue();
    assertThat(message).isNotNull();
    assertThat(message.getMessageType()).isEqualTo(MessageType.OK);
  }

  @Test(timeout = 10000L)
  public void should_receive_message() {
    prod.sendToAll(Message.builder().id(prod.getId()).messageType(MessageType.ACQUIRE_LOCK).build());

    Message message = cons.receiveMessage();

    assertThat(message).isNotNull();
    assertThat(message.getId()).isEqualTo(prod.getId());
    assertThat(message.getMessageType()).isEqualTo(MessageType.ACQUIRE_LOCK);
  }
}
