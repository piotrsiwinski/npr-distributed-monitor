package infiniteexample;

import pl.siwinski.piotr.api.monitor.Monitor;
import pl.siwinski.piotr.api.monitor.MonitorService;
import pl.siwinski.piotr.config.Config;

import java.util.concurrent.locks.Lock;

class Consumer {

  public static void main(String[] args) throws InterruptedException {
    Monitor monitor = MonitorService.create(
        Config.builder()
            .hostAddress("tcp://*:5001")
            .nodeAddress("tcp://*:5000")
            .ioThreads(10)
            .build()
    );
    System.out.println("Starting consumer process");
    Lock lock = monitor.getLock();

    while (true){
      System.out.println("Trying to get LOCK");
      lock.lock();
      Thread.sleep(3000);
      System.out.println("RELEASING LOCK ");
      lock.unlock();
    }



  }

}
