package producerconsumer.cons;

import pl.siwinski.piotr.api.monitor.Monitor;
import pl.siwinski.piotr.api.monitor.MonitorService;
import pl.siwinski.piotr.config.Config;

import java.util.Optional;

public class ConsProgram {
  public static void main(String[] args) {
    Monitor monitor =
        MonitorService.create(
            Config.builder()
                .hostAddress("tcp://*:5000")
                .nodeAddress("tcp://*:5001")
                .ioThreads(10)
                .bufferSize(10)
                .build());
    System.out.println("Starting consumer process");

    Consumer consumer = new Consumer(monitor);
    while (true) {
      Optional<Integer> consumed = consumer.consume();
      System.out.println(consumed.map(value -> "consumed " + value).orElse("Nothing to consume"));
      try {
        Thread.sleep(500);
      } catch (InterruptedException e) {
        e.printStackTrace();
      }
    }
  }
}
