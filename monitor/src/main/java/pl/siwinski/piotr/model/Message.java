package pl.siwinski.piotr.model;

import pl.siwinski.piotr.exception.MessageMissingNodeIdException;

import java.util.Objects;

public class Message {
  private String id;
  private Integer requestNumber;
  private MessageType messageType;
  private Token token;

  private Message(String id, Integer requestNumber, MessageType messageType, Token token) {
    this.id = id;
    this.requestNumber = requestNumber;
    this.messageType = messageType;
    this.token = token;
  }

  public String getId() {
    return id;
  }

  public Integer getRequestNumber() {
    return requestNumber;
  }

  public MessageType getMessageType() {
    return messageType;
  }

  public Token getToken() {
    return token;
  }

  @Override
  public String toString() {
    return String.format(
        "Message{id='%s', requestNumber=%d, messageType=%s, token=%s}",
        id, requestNumber, messageType, token);
  }

  public static MessageBuilder builder() {
    return new MessageBuilder();
  }

  public static class MessageBuilder {

    private String id;
    private Integer requestNumber;
    private MessageType messageType;
    private Token token;

    public MessageBuilder id(String id) {
      this.id = id;
      return this;
    }

    public MessageBuilder requestNumber(Integer requestNumber) {
      this.requestNumber = requestNumber;
      return this;
    }

    public MessageBuilder messageType(MessageType messageType) {
      this.messageType = messageType;
      return this;
    }

    public MessageBuilder token(Token token) {
      this.token = token;
      return this;
    }

    public Message build() {
      if (Objects.isNull(id)) {
        throw new MessageMissingNodeIdException();
      }
      return new Message(id, requestNumber, messageType, token);
    }
  }
}
