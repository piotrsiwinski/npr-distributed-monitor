import org.zeromq.ZMQ;

public class PushPlayground2 {

  public static void main(String[] args) throws InterruptedException {
    ZMQ.Context context = ZMQ.context(10);
    ZMQ.Socket sender = context.socket(ZMQ.PUSH);
    sender.connect("tcp://*:5000");
//    System.out.println("PULL - Listening on port - 5000");


    while (true) {
      System.out.println("Sending message");
      Thread.sleep(3000);
      sender.send("MESSAGE - NODE 2");
    }


  }
}
