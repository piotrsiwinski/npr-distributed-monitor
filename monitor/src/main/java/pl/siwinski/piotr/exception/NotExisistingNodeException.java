package pl.siwinski.piotr.exception;

public class NotExisistingNodeException extends MonitorException {
  public NotExisistingNodeException() {
  }

  public NotExisistingNodeException(String message) {
    super(message);
  }
}
