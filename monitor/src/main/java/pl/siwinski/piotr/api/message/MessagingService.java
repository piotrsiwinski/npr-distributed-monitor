package pl.siwinski.piotr.api.message;

import com.google.common.collect.ImmutableList;
import java.io.IOException;
import java.net.InetSocketAddress;
import java.net.MulticastSocket;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;
import java.util.List;

public class MessagingService {

  private ServerSocket serverSocket;
  private List<String> nodes = new ImmutableList.Builder<String>()
      .add("tcp://*:5000")
      .add("tcp://*:5001")
      .add("tcp://*:5002")
      .add("tcp://*:5003")
      .build();

  public MessagingService() {

  }

  public void setup(){

  }

  public void sendMessage() {

  }

  public void receiveMessage() {

  }
}
