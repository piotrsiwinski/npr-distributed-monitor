package pl.siwinski.piotr.exception;

public class IllegalMonitorStateException extends RuntimeException {
  public IllegalMonitorStateException() {
  }

  public IllegalMonitorStateException(String message) {
    super(message);
  }
}
