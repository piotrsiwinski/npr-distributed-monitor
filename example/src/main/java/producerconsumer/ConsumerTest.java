package producerconsumer;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import pl.siwinski.piotr.api.monitor.Monitor;
import pl.siwinski.piotr.api.monitor.MonitorService;
import pl.siwinski.piotr.config.Config;
import producerconsumer.cons.Consumer;
import producerconsumer.prod.Producer;

import java.util.Arrays;
import java.util.Optional;
import java.util.stream.IntStream;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.MatcherAssert.assertThat;

public class ConsumerTest {

  Monitor consumerMonitor;
  Monitor producerMonitor;
  Producer producer;
  Consumer consumer;

  @Before
  public void beforeAll() {
    consumerMonitor = getConsumerMonitor();
    producerMonitor = getProducerMonitor();
    producer = new Producer(producerMonitor);
    consumer = new Consumer(consumerMonitor);
  }

  @After
  public void after() {
    consumerMonitor.close();
    producerMonitor.close();
  }

  @Test
  public void should_produce_one_value_and_consume_one_value() {
    Integer valueToProduce = 5;
    producer.produce(valueToProduce);

    Integer consume = consumer.consume().get();

    assertThat(consume, equalTo(valueToProduce));
    assertThat(consumer.consume().isPresent(), equalTo(false));
  }

  @Test
  public void should_not_produce_when_buffer_full() {
    for (int i = 0; i < 10; i++) {
      boolean result = producer.produce(i);
      if (!result) {
        throw new RuntimeException("fail test - producer cant produce, even if not full buffer");
      }
    }
    boolean produceResult = producer.produce(11);
    assertThat(produceResult, equalTo(false));
  }

  @Test
  public void should_not_consume_empty_buffer() {
    Optional<Integer> consume = consumer.consume();
    assertThat(consume.isPresent(), equalTo(false));
  }

  @Test
  public void should_produce_full_buffer_and_consume_it() {
    int[] ints = IntStream.range(0, 10).toArray();
    Arrays.stream(ints).forEach(e -> producer.produce(e));

    int[] consumedArray = new int[ints.length];
    for (int i = 0; i < ints.length; i++) {
      consumedArray[i] = consumer.consume().get();
    }

    assertThat(ints, equalTo(consumedArray));
  }

  private Monitor getProducerMonitor() {
    return MonitorService.create(
        Config.builder()
            .hostAddress("tcp://*:5001")
            .nodeAddress("tcp://*:5000")
            .ioThreads(10)
            .bufferSize(10)
            .build());
  }

  private Monitor getConsumerMonitor() {
    return MonitorService.create(
        Config.builder()
            .hostAddress("tcp://*:5000")
            .nodeAddress("tcp://*:5001")
            .ioThreads(10)
            .bufferSize(10)
            .build());
  }
}
