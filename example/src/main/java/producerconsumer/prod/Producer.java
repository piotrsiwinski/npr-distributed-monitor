package producerconsumer.prod;

import pl.siwinski.piotr.api.monitor.Monitor;
import pl.siwinski.piotr.model.Buffer;

import java.util.Objects;
import java.util.concurrent.locks.Lock;

public class Producer {

  private final Monitor monitor;
  private int producerIndex = 0;

  public Producer(Monitor monitor) {
    this.monitor = Objects.requireNonNull(monitor);
  }

  public boolean produce(Integer value) {
    Lock lock = monitor.getLock();
    lock.lock();
    Buffer buffer = monitor.getBuffer();

    if (buffer.isFull()) {
      System.out.println("Nothing to produce");
      try {
        Thread.sleep(5000);
      } catch (InterruptedException e) {
        e.printStackTrace();
      }
      lock.unlock();
      return false;
    }

    System.out.println("Produced " + value);
    buffer.add(value, producerIndex);
    producerIndex = (producerIndex + 1) % buffer.getBufferSize();

    System.out.println("Releasing lock");
    lock.unlock();
    return true;
  }
}
