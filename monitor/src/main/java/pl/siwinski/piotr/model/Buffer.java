package pl.siwinski.piotr.model;

import java.util.ArrayList;
import java.util.List;

public class Buffer {
  private int[] buffer;
  private int bufferIndex;
  private int bufferSize;

  Buffer(int bufferSize) {
    this.bufferSize = bufferSize;
    this.bufferIndex = 0;
    this.buffer = new int[bufferSize];
  }

  public void add(Integer value, Integer index) {
    this.buffer[index] = value;
    bufferIndex++;
  }

  public Integer remove(int index) {
    Integer remove = this.buffer[index];
    bufferIndex--;
    return remove;
  }

  public boolean isFull() {
    return bufferSize == bufferIndex;
  }

  public boolean isEmpty() {
    return bufferIndex == 0;
  }

  public int getBufferSize() {
    return this.bufferSize;
  }
}
