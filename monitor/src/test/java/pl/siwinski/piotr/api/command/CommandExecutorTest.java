package pl.siwinski.piotr.api.command;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import pl.siwinski.piotr.config.Config;
import pl.siwinski.piotr.model.Node;

import java.util.concurrent.CompletableFuture;

public class CommandExecutorTest {

  private CommandExecutor consumerExecutor;
  private CommandExecutor producerExecutor;

  @Before
  public void setUp() {
    consumerExecutor =
        new CommandExecutor(
            new Node(
                Config.builder()
                    .hostAddress("tcp://*:5001")
                    .nodeAddress("tcp://*:5000")
                    .ioThreads(10)
                    .build()));

    producerExecutor =
        new CommandExecutor(
            new Node(
                Config.builder()
                    .hostAddress("tcp://*:5000")
                    .nodeAddress("tcp://*:5001")
                    .ioThreads(10)
                    .build()));
  }

  @After
  public void cleanUp() {
    producerExecutor.shutdown();
    consumerExecutor.shutdown();
  }

  @Test(timeout = 10000L)
  public void acquireLock() {
    producerExecutor.acquireLock();
    producerExecutor.releaseLock();
  }

  @Test(timeout = 10000L)
  public void should_two_acquire_locks() {
    producerExecutor.acquireLock();
    producerExecutor.releaseLock();
    consumerExecutor.acquireLock();
    consumerExecutor.releaseLock();
  }

  @Test(timeout = 10000L)
  public void should_two_acquire_locks2() {
    producerExecutor.acquireLock();
    CompletableFuture.runAsync(
        () -> {

          //              try {
          //                Thread.sleep(3000);
          //              } catch (InterruptedException e) {
          //                e.printStackTrace();
          //              }
          System.out.println("producerExecutor.releaseLock()");
          producerExecutor.releaseLock();
        });
    System.out.println("consumerExecutor - Acquire lock ");
    consumerExecutor.acquireLock();
    consumerExecutor.releaseLock();
  }
}
