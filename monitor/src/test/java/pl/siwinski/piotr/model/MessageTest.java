package pl.siwinski.piotr.model;

import org.junit.Test;
import pl.siwinski.piotr.exception.MessageMissingNodeIdException;

public class MessageTest {

  @Test(expected = MessageMissingNodeIdException.class)
  public void shouldThrowExceptionWhenMessageWithoutId() {
    Message.builder().build();
  }
}
