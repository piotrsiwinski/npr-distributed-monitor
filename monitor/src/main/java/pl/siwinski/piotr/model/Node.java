package pl.siwinski.piotr.model;

import com.google.gson.Gson;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.zeromq.ZMQ;
import org.zeromq.ZMQ.Context;
import org.zeromq.ZMQ.Socket;
import pl.siwinski.piotr.api.command.CommandExecutor;
import pl.siwinski.piotr.config.Config;
import pl.siwinski.piotr.exception.NotExisistingNodeException;

import java.io.IOException;
import java.nio.channels.ClosedSelectorException;
import java.util.*;

public class Node {
  private Logger logger = LoggerFactory.getLogger(CommandExecutor.class);
  private Map<String, Integer> requestsNumberMap = new HashMap<>();
  private String id;
  private String hostAddress;

  private Context context;
  private Socket pullSocket;
  private List<Socket> pushSockets = new ArrayList<>();
  private Token token;

  public Node(Config config) {
    this.hostAddress = config.getHostAddress();
    this.context = ZMQ.context(config.getIoThreads());
    this.pullSocket = context.socket(ZMQ.PULL);
    this.pullSocket.bind(this.hostAddress);
    this.id = getIdFromSocket(pullSocket);
    config.getNodeAddresses().forEach(s -> pushSockets.add(this.context.socket(ZMQ.PUSH)));
    config.getNodeAddresses().forEach(addr -> pushSockets.forEach(socket -> socket.connect(addr)));

    this.requestsNumberMap.put(id, 0);
    pushSockets.forEach(s -> this.requestsNumberMap.put(getIdFromSocket(s), 0));

    // todo: fix
    if (id.equals("5001")) {
      token = new Token(config);
    }
    logger.info("Starting node with parameters: {}", this.toString());
  }

  public String getId() {
    return id;
  }

  private String getIdFromSocket(Socket s) {
    return s.getLastEndpoint().split(":")[2];
  }

  private Socket getPushSocketById(String id) {
    return this.pushSockets
        .stream()
        .filter(s -> s.getLastEndpoint().contains(id))
        .findFirst()
        .orElseThrow(NotExisistingNodeException::new);
  }

  public Map<String, Integer> getRequestsNumberMap() {
    return requestsNumberMap;
  }

  public Message receiveMessage() {
    Gson gson = new Gson();
    return gson.fromJson(new String(this.pullSocket.recv(0)), Message.class);
  }

  public boolean sendToAll(Message message) {
    Gson gson = new Gson();
    boolean result = false;
    for (Socket s : pushSockets) {
      result = s.send(gson.toJson(message), 0);
      if (!result) {
        throw new RuntimeException("Cannot send message to: " + s.getLastEndpoint());
      }
    }
    return result;
  }

  public boolean sendTo(String id, Message message) {
    if (id == null) {
      throw new IllegalArgumentException("Sender id cannot be null");
    }
    Gson gson = new Gson();
    return getPushSocketById(id).send(gson.toJson(message));
  }

  public Token getToken() {
    return token;
  }

  public void setToken(Token token) {
    this.token = token;
  }

  public Integer incrementAndGetHostRequestNumber() {
    Integer oldValue = getRequestsNumberMap().get(id);
    this.requestsNumberMap.put(id, ++oldValue);
    return this.requestsNumberMap.get(id);
  }

  public void shutdown() {
    this.pullSocket.close();

    this.pushSockets.forEach(Socket::close);
    //todo: fix closing exception
//    this.context.close();
  }

  @Override
  public String toString() {
    StringBuilder stringBuilder = new StringBuilder();
    for (Socket s : pushSockets) {
      stringBuilder.append(s.getLastEndpoint());
    }
    return String.format(
        "Node {id='%s', hostAddress='%s', pushSockets=%s, withToken = '%s'}",
        id, hostAddress, stringBuilder.toString(), !Objects.isNull(token));
  }
}
