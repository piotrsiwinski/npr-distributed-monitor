package producerconsumer.cons;

import pl.siwinski.piotr.api.monitor.Monitor;
import pl.siwinski.piotr.model.Buffer;

import java.util.Objects;
import java.util.Optional;
import java.util.concurrent.locks.Lock;

public class Consumer {
  private final Monitor monitor;
  private int consumerIndex = 0;

  public Consumer(Monitor monitor) {
    this.monitor = Objects.requireNonNull(monitor);
  }

  public Optional<Integer> consume() {
    Lock lock = monitor.getLock();
    lock.lock();
    Buffer buffer = monitor.getBuffer();

    if (buffer.isEmpty()) {
      System.out.println("Nothing to should_consume_one_value");
      lock.unlock();
      return Optional.empty();
    }

    Integer consumedValue = buffer.remove(consumerIndex);
    System.out.println("Consumed" + consumedValue);
    consumerIndex = (consumerIndex + 1) % buffer.getBufferSize();

    lock.unlock();
    System.out.println("Releasing lock");
    return Optional.of(consumedValue);
  }
}
