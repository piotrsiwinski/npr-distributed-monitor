package pl.siwinski.piotr.api.command;

import pl.siwinski.piotr.model.Message;

public interface CommandReceiver {
  String onCommandReceived(Message message);

}
